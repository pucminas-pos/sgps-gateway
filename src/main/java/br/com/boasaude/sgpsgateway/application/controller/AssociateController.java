package br.com.boasaude.sgpsgateway.application.controller;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.LoginRequest;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.User;
import br.com.boasaude.sgpsgateway.domain.service.AssociateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("associates")
@RequiredArgsConstructor
public class AssociateController {

    private final AssociateService associateService;


    @PostMapping("login")
    public ResponseEntity login(@RequestBody LoginRequest loginRequest) {
        return ResponseEntity.ok(associateService.login(loginRequest));
    }

    @PostMapping
    public ResponseEntity save(@RequestBody User user) {
        return ResponseEntity.status(HttpStatus.CREATED).body(associateService.save(user));
    }

    @GetMapping("email")
    public ResponseEntity findById(@RequestParam String email) {
        return ResponseEntity.ok(associateService.findByEmail(email));
    }
}

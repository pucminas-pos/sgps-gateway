package br.com.boasaude.sgpsgateway.application.controller;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanCategory;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanName;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanType;
import br.com.boasaude.sgpsgateway.domain.service.PlanService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("plans")
public class PlanController {

    private final PlanService planService;

    @GetMapping
    public ResponseEntity findAll() {
        return ResponseEntity.ok(planService.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(planService.findById(id));
    }

    @GetMapping(value = "filter", params = {"name", "category", "type"})
    public ResponseEntity findByNameAndCategoryAndType(@RequestParam Map<String, String> requestParam) {
        PlanName planName = PlanName.valueOf(requestParam.get("name"));
        PlanCategory planCategory = PlanCategory.valueOf(requestParam.get("category"));
        PlanType planType = PlanType.valueOf(requestParam.get("type"));

        return ResponseEntity.ok(planService.findByFilter(planName, planCategory, planType));
    }
}

package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

public enum PlanType {

    MEDICAL, MEDICAL_ODONTOLOGY

}

package br.com.boasaude.sgpsgateway.domain.service.impl;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.Associate;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.LoginRequest;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.User;
import br.com.boasaude.sgpsgateway.domain.service.AssociateService;
import br.com.boasaude.sgpsgateway.infrastructure.api.SgpsAPI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class AssociateServiceImpl implements AssociateService {

    private final SgpsAPI sgpsAPI;

    @Override
    public Associate findByEmail(String email) {
        return sgpsAPI.findByEmail(email);
    }

    @Override
    public Associate login(LoginRequest loginRequest) {
        return sgpsAPI.login(loginRequest);
    }

    @Override
    public Associate save(User user) {
        return sgpsAPI.save(user);
    }

}
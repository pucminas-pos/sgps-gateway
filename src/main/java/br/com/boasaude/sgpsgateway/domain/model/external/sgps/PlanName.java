package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

public enum PlanName {

    INDIVIDUAL, CORPORATE;

}

package br.com.boasaude.sgpsgateway.domain.model.internal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExceptionMessage implements Serializable {

    private String message;
    private String date;

}

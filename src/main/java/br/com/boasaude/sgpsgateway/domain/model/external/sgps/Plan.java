package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Plan implements Serializable {

    private Long id;
    private PlanName name;
    private PlanType type;
    private PlanCategory category;
}

package br.com.boasaude.sgpsgateway.domain.service;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.Plan;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanCategory;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanName;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanType;

import java.util.List;

public interface PlanService {

    List<Plan> findAll();

    Plan findById(Long id);

    Plan findByFilter(PlanName name, PlanCategory category, PlanType type);
}

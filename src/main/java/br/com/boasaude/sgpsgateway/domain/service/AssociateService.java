package br.com.boasaude.sgpsgateway.domain.service;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.Associate;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.LoginRequest;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.User;

public interface AssociateService {

    Associate findByEmail(String email);

    Associate login(LoginRequest loginRequest);

    Associate save(User user);

}

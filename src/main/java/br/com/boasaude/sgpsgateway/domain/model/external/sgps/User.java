package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "role", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Associate.class, name = "ASSOCIATE"),
        @JsonSubTypes.Type(value = User.class, name = "ADMIN")
})
public class User {

    private Long id;
    private String fullname;
    private String email;
    private String password;
    private String rg;
    private String cpf;
    private String telephone;
    private String birthdate;
    private UserRole role;
    private LocalDateTime createdAt;
    private LocalDateTime updatedAt;

}

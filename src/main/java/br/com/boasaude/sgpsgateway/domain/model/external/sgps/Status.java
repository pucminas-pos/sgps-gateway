package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

public enum Status {

    ACTIVE, SUSPENDED, INACTIVE

}

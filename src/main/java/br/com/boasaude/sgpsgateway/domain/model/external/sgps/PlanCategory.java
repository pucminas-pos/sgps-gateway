package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

public enum PlanCategory {

    BASIC, INTERMEDIATE, VIP

}

package br.com.boasaude.sgpsgateway.domain.service.impl;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.Plan;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanCategory;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanName;
import br.com.boasaude.sgpsgateway.domain.model.external.sgps.PlanType;
import br.com.boasaude.sgpsgateway.domain.service.PlanService;
import br.com.boasaude.sgpsgateway.infrastructure.api.SgpsAPI;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class PlanServiceImpl implements PlanService {

    private final SgpsAPI sgpsAPI;


    @Override
    public List<Plan> findAll() {
        return sgpsAPI.findAllPlans();
    }

    @Override
    public Plan findById(Long id) {
        return sgpsAPI.findPlanById(id);
    }

    @Override
    public Plan findByFilter(PlanName name, PlanCategory category, PlanType type) {
        return sgpsAPI.findPlanByNameAndCategoryAndType(name, category, type);
    }
}

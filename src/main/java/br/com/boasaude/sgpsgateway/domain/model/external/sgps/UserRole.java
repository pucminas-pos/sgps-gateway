package br.com.boasaude.sgpsgateway.domain.model.external.sgps;

public enum UserRole {

    ADMIN, ASSOCIATE
}

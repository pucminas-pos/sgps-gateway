package br.com.boasaude.sgpsgateway.infrastructure.api.pool;

import feign.Client;
import feign.Request;
import feign.httpclient.ApacheHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

public class SgpsPoolConfig {

    @Value("${application.sgps.pool.maxConn}")
    private Integer maxConn;

    @Value("${application.sgps.pool.maxRoute}")
    private Integer maxRoute;

    @Value("${application.sgps.pool.connTimeout}")
    private Integer connTimeout;

    @Value("${application.sgps.pool.readTimeout}")
    private Integer readTimeout;

    @Bean
    public Request.Options options() {
        return new Request.Options(connTimeout, readTimeout);
    }

    @Bean
    public Client poolConfig() {
        return new ApacheHttpClient(
                HttpClientBuilder.create().setMaxConnPerRoute(maxRoute).setMaxConnTotal(maxConn).build());
    }
}

package br.com.boasaude.sgpsgateway.infrastructure.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class IntegrationException extends RuntimeException {

    private HttpStatus httpStatus;

    public IntegrationException(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public IntegrationException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }

    public IntegrationException(String message, Throwable cause, HttpStatus httpStatus) {
        super(message, cause);
        this.httpStatus = httpStatus;
    }

}

package br.com.boasaude.sgpsgateway.infrastructure.api;

import br.com.boasaude.sgpsgateway.domain.model.external.sgps.*;
import br.com.boasaude.sgpsgateway.domain.model.internal.ExceptionMessage;
import br.com.boasaude.sgpsgateway.infrastructure.api.pool.SgpsPoolConfig;
import br.com.boasaude.sgpsgateway.infrastructure.exception.*;
import com.google.gson.Gson;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.List;

@FeignClient(value = "sgps-api", url = "${application.sgps.url}",
        configuration = {SgpsAPI.SgpsErrorDecoder.class, SgpsPoolConfig.class})
public interface SgpsAPI {

    @GetMapping("associates/email")
    Associate findByEmail(@RequestParam String email);

    @PostMapping("associates/login")
    Associate login(@RequestBody LoginRequest loginRequest);

    @PostMapping("associates")
    Associate save(@RequestBody User user);

    @GetMapping("plans")
    List<Plan> findAllPlans();

    @GetMapping("plans/{id}")
    Plan findPlanById(@PathVariable("id") Long id);

    @GetMapping("plans/filter")
    Plan findPlanByNameAndCategoryAndType(@RequestParam("name") PlanName name,
                                          @RequestParam("category") PlanCategory category,
                                          @RequestParam("type") PlanType type);


    class SgpsErrorDecoder implements ErrorDecoder {

        @Autowired
        public Gson gson;

        @Override
        public Exception decode(String method, Response response) {
            final HttpStatus status = HttpStatus.valueOf(response.status());

            if (status == HttpStatus.INTERNAL_SERVER_ERROR)
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            try {
                Reader reader = response.body().asReader(StandardCharsets.UTF_8);
                ExceptionMessage exceptionMessage = gson.fromJson(reader, ExceptionMessage.class);
                return new IntegrationException(exceptionMessage.getMessage(), status);

            } catch (Exception ex) {
                return new IntegrationException("integration error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    }
}

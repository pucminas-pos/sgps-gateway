package br.com.boasaude.sgpsgateway.infrastructure.exception;

import org.springframework.http.HttpStatus;

public class SgpsIntegrationException extends IntegrationException {

    public SgpsIntegrationException(String message, HttpStatus httpStatus) {
        super(message, httpStatus);
    }
}

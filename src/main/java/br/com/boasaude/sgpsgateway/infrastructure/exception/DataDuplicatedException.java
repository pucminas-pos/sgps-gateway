package br.com.boasaude.sgpsgateway.infrastructure.exception;

public class DataDuplicatedException extends RuntimeException {

    public DataDuplicatedException() {
    }

    public DataDuplicatedException(String message) {
        super(message);
    }

    public DataDuplicatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataDuplicatedException(Throwable cause) {
        super(cause);
    }
}

package br.com.boasaude.sgpsgateway.infrastructure.exception;

public class AssociateNotFoundException extends RuntimeException {

    public AssociateNotFoundException() {
    }

    public AssociateNotFoundException(String message) {
        super(message);
    }

    public AssociateNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public AssociateNotFoundException(Throwable cause) {
        super(cause);
    }
}

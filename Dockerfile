FROM maven:3.6.0-jdk-11-slim AS build
COPY src /home/sgps-gateway/src
COPY pom.xml /home/sgps-gateway
RUN mvn -f /home/sgps-gateway/pom.xml clean package

FROM adoptopenjdk/openjdk11:alpine
COPY --from=build /home/sgps-gateway/target/sgps-gateway.jar /usr/local/lib/sgps-gateway.jar
EXPOSE 8085
ENTRYPOINT ["java","-jar","/usr/local/lib/sgps-gateway.jar"]